"""
Testing settings and globals. Used for Jenkins.
"""
from base import *  # pylint: disable=W0401,W0614
import os

SOUTH_TESTS_MIGRATE = False

INSTALLED_APPS += (
    'django_jenkins',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3'
    }
}

JENKINS_TASKS = {
    'django_jenkins.tasks.run_jshint',
    'django_jenkins.tasks.run_csslint',
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pep8'
}

PROJECT_APPS = {
    '{{ project_name }}',
}

PYLINT_RCFILE = os.path.join(DJANGO_ROOT, 'settings/lintrc/.pylintrc')
PEP8_RCFILE = os.path.join(DJANGO_ROOT, 'settings/lintrc/.pep8rc')
