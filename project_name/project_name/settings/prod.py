"""
Production settings and globals.
"""

from base import *  # pylint: disable=W0401,W0614

########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ db_name }}_prod',
        'USER': '{{ db_user }}',
        'PASSWORD': '{{ db_password }}',
        'HOST': '',
        'PORT': '',
    }
}
########## END DATABASE CONFIGURATION

########## HOST CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/releases/1.5/#allowed-hosts-required-in-production
from allowed_hosts import ALLOWED_HOSTS  # pylint: disable=F0401,W0611
########## END HOST CONFIGURATION

########## RAVEN / SENTRY CONFIGURATION
INSTALLED_APPS += (
    'raven.contrib.django.raven_compat',
)

MIDDLEWARE_CLASSES += (
    'raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware',
    'raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',
)

RAVEN_CONFIG = {
    'dsn': '{{ sentry_host }}',
    'site': '{{ project_name }}',
    'name': 'Prod',
    'auto_log_stacks': True
}
########## END RAVEN / SENTRY CONFIGURATION

########## LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': ' %(asctime)s %(levelname)s %(name)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['require_debug_false'],
            'formatter': 'verbose'
        },
        'sentry': {
            'level': 'INFO',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['sentry', 'mail_admins'],
            'propagate': True,
            'level': 'ERROR',
        },
        'huey.consumer': {
            'handlers': ['sentry', 'mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        '{{ project_name }}': {
            'handlers': ['sentry'],
            'propagate': True,
            'level': 'INFO',
        },
    }
}

########## END LOGGING CONFIGURATION

########## HUEY CONFIGURATION
INSTALLED_APPS += (
    'huey.djhuey',
)

HUEY = {
    'backend': 'huey.backends.redis_backend',
    'name': '{{ project_name }}_prod',
    'connection': {'host': '{{ redis_host }}', 'port': '{{ redis_port }}', 'password': '{{ redis_password }}'},
    'always_eager': False,
    'consumer_options': {'workers': 1},
}
########## END HUEY CONFIGURATION

########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': '{{ project_name }}_cache_prod',
    }
}

#Does not cache when logged in
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True
CACHE_MIDDLEWARE_SECONDS = 3600
CACHE_MIDDLEWARE_KEY_PREFIX = '{{ project_name }}'
########## END CACHE CONFIGURATION
