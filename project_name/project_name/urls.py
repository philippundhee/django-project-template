from django.conf.urls import *  # pylint: disable=W0401,W0614
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings
from cms.sitemaps import CMSSitemap
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = i18n_patterns('',
                            url(r'^admin/', include(admin.site.urls)),
                            url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
                                {'sitemaps': {'cmspages': CMSSitemap}}),
                            url(r'^', include('cms.urls')),
                            (r'^robots\.txt$',
                             TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
                            url(r'^', include('favicon.urls')),
)

if settings.DEBUG:
    urlpatterns = patterns('',
                           url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                               {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                           url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
                            url(r'^rosetta/', include('rosetta.urls')),
    )
