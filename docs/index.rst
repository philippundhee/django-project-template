
Welcome to the {{project_name}} documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   workflow
   tests



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
