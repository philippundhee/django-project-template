Install
=========

First you need to check out the dev branch:

.. code-block:: bash

    git checkout dev

To create the virtualenv and install the necessary dependencies run the bootstrap script:

.. code-block:: bash

    ./bootstrap dev

In order to be able to use the packages installed, activate the virtualenv:

.. code-block:: bash

    source bin/activate


Set up the database. This assumes that postgresql is already installed on your pc and running:

.. code-block:: bash

    fab setup_database

You can now import the latest database dump:

.. code-block:: bash

    fab restore


You can now start the django runserver:

.. code-block:: bash

    cd {{ project_name }}
    python manage.py runserver

