Workflow
========

Branching model
-----------------

All development is done in the dev branch and is then deployed to the acceptance branch. The acceptance branch is used
by the client to greenlight changes for production. If the client is happy with what is on the acceptance branch,
the acceptance branch is deployed to the production branch.

Git
-----------------

Nothing special. Changes to the code are committed to the git repository and always pushed:

.. code-block:: bash

    git commit -am "Site does not cause kittens to die anymore."
    git push

Creating and restoring backups
-----------------

To create a backup of your local development database run:

.. code-block:: python

    fab backup

This will backup your local db, but not the media files. If you want to include them as well, run:

.. code-block:: python

    fab backup:media=True

To restore your database to a backup run:

.. code-block:: python

    fab restore

Here as well, if you want the media files, you have to specify it:

.. code-block:: python

    fab restore:media=True

If you want to include a database dump from the production or acceptance branch, for example when you need real data, run:

.. code-block:: python

    fab restore:source_branch=accpt


or

.. code-block:: python

    fab restore:source_branch=prod

Again, if you want the media files you have to pass media=True as an option.

Deployment
------------

Deploying is easy, just run:

.. code-block:: python

    fab deploy:accpt

Documentation
--------------

If you made any changes to the documentation, run

.. code-block:: bash

    make html

in the docs directory and commit the changes.
